# http-monitor

http-monitor is a web service monitoring app built using django framework.  

The app utilises **requests** library to make http GET requests to each web service.

If the response of the GET requests is a 200, then everything is fine, else, then an error has occured.

---------------------------------------------------------------------------

## Requirements

Python 3

Django 1.11

Requests 2.18.4

database = Mysql MariaDB 

--------------------------------------------------------------------------

## Configuration for database export

DATABASE NAME --> monitor

USERNAME --> Ehealth

Password --> e4e2018

--------------------------------------------------------------------------

## Outstanding features to include

1) Error notification of why an error occured and when it happened

2) Email for notification to remote users








  