from django.conf.urls import url

from . import views

app_name = 'pinger'
urlpatterns = [
        url(r'^add/$',views.add, name = 'add'),
        url(r'^$',views.index,name = 'index'),
        url(r'^remove/(?P<name>\w+)/$', views.remove, name = 'remove'),
        url(r'^pause/(?P<name>\w+)/$', views.pause, name = 'pause'),
        url(r'^restart/(?P<name>\w+)/$',views.restart, name = 'restart'),
        url(r'^rename/(?P<name>\w+)/$',views.rename, name = 'rename'),
        url(r'^(?P<name>\w+)/$', views.detail, name = 'detail'),
        
        ]

